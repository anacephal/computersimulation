﻿using System;
using System.Collections.Generic;

namespace CPU
{
    public static class Helpers
    {
        public static bool AreSequenceEqual<T>(IEnumerable<T> a, IEnumerable<T> b, Func<T, T, bool> comparator)
        {
            var enumA = a.GetEnumerator();
            var enumB = b.GetEnumerator();
            while (enumA.MoveNext())
            {
                enumB.MoveNext();
                if (!comparator(enumA.Current, enumB.Current))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
