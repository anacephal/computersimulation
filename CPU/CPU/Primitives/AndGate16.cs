﻿using System;
namespace CPU.Primitives
{
    public class AndGate16 : Chip
    {
        public void Compute(bool[] a, bool[] b)
        {
            var result = new bool[16];
            for(int i=0; i<16; i++)
            {
                result[i] = a[i] == b[i] && a[i] == true;
            }
            Out = result;
        }
    }
}
