﻿using System;
namespace CPU.Primitives
{
    public class NotAndGate
    {
        public bool Out { get; private set; }

        public void Compute(bool a, bool b)
        {
            var and = new AndGate();
            var not = new NotGate();
            and.Compute(a, b);
            not.Compute(and.Out);
            Out = not.Out;
        }
    }
}
