﻿using System;
namespace CPU.Primitives
{
    public class NotGate16 : Chip
    {
        public void Compute(bool[] source)
        {
            var result = new bool[source.Length];
            for (var i = 0; i < source.Length; i++)
            {
                result[i] = !source[i];
            }
            Out = result;
        }
    }
}
