﻿using System;
namespace CPU.Primitives
{
    public class NotGate
    {
        public bool Out { get; private set; }

        public void Compute(bool a)
        {
            Out = !a;
        }
    }
}
