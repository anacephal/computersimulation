﻿using System;
namespace CPU.Primitives
{
    public class MuxGate
    {
        public bool Out { get; private set; }

        public void Compute(bool a, bool b, bool sel)
        {
            // a b sel out
            // 0 0 0 0
            // 0 1 0 0
            // 1 0 0 1
            // 1 1 0 1
            // 0 0 1 0
            // 0 1 1 1
            // 1 0 1 0
            // 1 1 1 1

            // a and !sel
            // b and sel
            var not = new NotGate();
            var and = new AndGate();
            var and2 = new AndGate();
            var or = new OrGate();

            not.Compute(sel);
            and.Compute(a, not.Out);
            and2.Compute(b, sel);
            or.Compute(and.Out, and2.Out);
            Out = or.Out;
        }
    }
}
