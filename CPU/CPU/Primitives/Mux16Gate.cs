﻿using System;
namespace CPU.Primitives
{
    public class Mux16Gate : Chip
    {
        public void Compute(bool[] a, bool[] b, bool sel)
        {
            var result = new bool[16];
            var mux = new MuxGate();

            for(var i=0;i< result.Length; i++)
            {
                mux.Compute(a[i], b[i], sel);
                result[i] = mux.Out;
            }
            Out = result;
        }
    }
}
