﻿using System;
using System.Linq;

namespace CPU.Primitives
{
    public class And16Gate
    {
        public bool Out { get; private set; }

        public void Compute(params bool[] a)
        {
            var andGate = new AndGate();
            Out = a.Aggregate(true, (acc, cur) =>
            {
                andGate.Compute(acc, cur);
                return andGate.Out;
            });
        }
    }
}
