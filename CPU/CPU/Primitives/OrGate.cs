﻿using System;
namespace CPU.Primitives
{
    public class OrGate
    {
        public bool Out { get; private set; }
        public void Compute(bool a, bool b)
        {
            Out = a || b;
        }
    }
}
