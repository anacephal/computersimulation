﻿using System;
namespace CPU.Primitives
{
    public class AndGate
    {
        public bool Out { get; private set; }

        public void Compute(bool a, bool b)
        {
            Out = a && b;
        }
    }
}
