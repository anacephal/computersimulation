﻿using System;

namespace CPU.Primitives
{
    public class Register16 : Chip
    {

        public Register16()
        {
            Out = new bool[16];
        }

        public void Compute(bool[] data, bool load)
        {
            if(load)
            {
                Out = data;
            }
        }
    }
}
