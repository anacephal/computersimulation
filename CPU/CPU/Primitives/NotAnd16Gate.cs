﻿using System;
namespace CPU.Primitives
{
    public class NotAnd16Gate
    {
        public bool Out { get; private set; }

        public void Compute(params bool[] a)
        {
            var andGate = new And16Gate();
            var notGate = new NotGate();
            andGate.Compute(a);
            notGate.Compute(andGate.Out);
            Out = notGate.Out;
        }
    }
}
