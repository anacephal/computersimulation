﻿using System;
namespace CPU.Primitives
{
    public abstract class Chip
    {
        public virtual bool[] Out { get; protected set; }
    }
}
