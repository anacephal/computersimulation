﻿using System;
using CPU.Complex;

namespace CPU
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //TestRam();
            //TestAlu();
            TestComputer();
        }

        private static void TestComputer()
        {
            var instructions = BooleanHelper.ConvertToInstructions(
                "0000000000010000" +
                "1110111111001000" +
                "0000000000010001" +
                "1110101010001000" +
                "0000000000010000" +
                "1111110000010000" +
                "0000000000000000" +
                "1111010011010000" +
                "0000000000010010" +
                "1110001100000001" +
                "0000000000010000" +
                "1111110000010000" +
                "0000000000010001" +
                "1111000010001000" +
                "0000000000010000" +
                "1111110111001000" +
                "0000000000000100" +
                "1110101010000111" +
                "0000000000010001" +
                "1111110000010000" +
                "0000000000000001" +
                "1110001100001000" +
                "0000000000010101" +
                "1110101010000111"
            );

            var ramState = BooleanHelper.ConvertToInstructions(
                "0000000000000111");

            var comp = new Computer(instructions, ramState);
            while (true)
            {
                comp.Compute();
                Console.WriteLine(BooleanHelper.ConvertToString(comp.Out));
            }
        }

        private static void TestAlu()
        {
            var alu = new ALUDecorator(new ALU());
            var a = BooleanHelper.ConvertToBool("0000 0000 0000 0011");
            var b = BooleanHelper.ConvertToBool("0001 1000 0100 0010");

            alu.Compute(a, b, AluOperations.XPlusY);
            var result = BooleanHelper.ConvertToString(alu.Out);

            Console.WriteLine(result);
        }

        private static void TestRam()
        {
            var ram = new RamDecorator(new Ram16k());
            var load = true;
            ram.Compute("0001 0000 0000 0000", load, "00 0000 1111 0000");
            ram.Compute(new bool[16], false, "0010");
            ram.Compute(new bool[16], false, "00 0000 1111 0000");

            var result = BooleanHelper.ConvertToString(ram.Out);
            Console.WriteLine(result);
        }
    }
}
