﻿using System;
using CPU.Primitives;

namespace CPU.Complex
{
    public class ALUDecorator : Chip
    {
        public bool Zr { get; private set; }
        public bool Ng { get; private set; }

        ALU _alu;

        public ALUDecorator(ALU alu)
        {
            _alu = alu;
        }

        public void Compute(bool[] x, bool[] y, AluOperations operation)
        {
            bool zx = false
                ,nx = false
                ,zy = false
                ,ny = false
                ,f = false
                ,no = false;

            switch(operation)
            {
                case AluOperations.Zero:
                    {
                        zx = true;
                        zy = true;
                        f = true;
                        break;
                    }
                case AluOperations.One:
                    {
                        zx = true;
                        nx = true;
                        zy = true;
                        ny = true;
                        f = true;
                        no = true;
                        break;
                    }
                case AluOperations.MinusOne:
                    {
                        zx = true;
                        nx = true;
                        zy = true;
                        f = true;
                        break;
                    }
                case AluOperations.X:
                    {
                        zy = true;
                        ny = true;
                        break;
                    }
                case AluOperations.Y:
                    {
                        zx = true;
                        nx = true;
                        break;
                    }
                case AluOperations.NotX:
                    {
                        zy = true;
                        ny = true;
                        no = true;
                        break;
                    }
                case AluOperations.NotY:
                    {
                        zx = true;
                        nx = true;
                        no = true;
                        break;
                    }
                case AluOperations.MinusX:
                    {
                        zy = true;
                        ny = true;
                        f = true;
                        no = true;
                        break;
                    }
                case AluOperations.MinusY:
                    {
                        zx = true;
                        nx = true;
                        f = true;
                        no = true;
                        break;
                    }
                case AluOperations.XPlusOne:
                    {
                        nx = true;
                        zy = true;
                        ny = true;
                        f = true;
                        no = true;
                        break;
                    }
                case AluOperations.YPlusOne:
                    {
                        zx = true;
                        nx = true;
                        ny = true;
                        f = true;
                        no = true;
                        break;
                    }
                case AluOperations.XMinusOne:
                    {
                        zy = true;
                        ny = true;
                        f = true;
                        break;
                    }
                case AluOperations.YMinusOne:
                    {
                        zx = true;
                        nx = true;
                        f = true;
                        break;
                    }
                case AluOperations.XPlusY:
                    {
                        f = true;
                        break;
                    }
                case AluOperations.XMinusY:
                    {
                        nx = true;
                        f = true;
                        no = true;
                        break;
                    }
                case AluOperations.YMinusX:
                    {
                        ny = true;
                        f = true;
                        no = true;
                        break;
                    }
                case AluOperations.XAndY:
                    {
                        break;
                    }
                case AluOperations.XOrY:
                    {
                        nx = true;
                        ny = true;
                        no = true;
                        break;
                    }
            }

            _alu.Compute(x, y, zx, nx, zy, ny, f, no);
            Out = _alu.Out;
            Ng = _alu.Ng;
            Zr = _alu.Zr;
        }
    }
}
