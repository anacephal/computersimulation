﻿using CPU.Complex.Adders;
using CPU.Primitives;

namespace CPU.Complex
{
    public class ProgramCounter : Chip
    {
        private readonly Register16 _register = new Register16();

        public void Compute(bool[] a, bool load, bool inc, bool reset)
        {
            //reset
            var mux16reset = new Mux16Gate();
            mux16reset.Compute(_register.Out, new bool[16], reset);

            //load
            var mux16load = new Mux16Gate();
            mux16load.Compute(mux16reset.Out, a, load);

            //inc
            var adder = new Adder16();
            // Add 1
            adder.Compute(_register.Out, new bool[] {
                    false, false, false, false,
                    false, false, false, false,
                    false, false, false, false,
                    false, false, false, true }
            );

            var mux16Add = new Mux16Gate();
            mux16Add.Compute(mux16load.Out, adder.Out, inc);

            _register.Compute(mux16Add.Out, true);
            Out = mux16Add.Out;
        }
    }
}
