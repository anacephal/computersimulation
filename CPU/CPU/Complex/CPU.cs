﻿using System;
using CPU.Primitives;

namespace CPU.Complex
{
    public class CPU
    {
        public bool WriteM { get; private set; }
        public bool[] OutM { get; private set; }
        public bool[] AddressM { get; private set; }
        public bool[] PC { get; private set; }

        private readonly Register16 _aRegister = new Register16();
        private readonly Register16 _dRegister = new Register16();
        private readonly ProgramCounter _pc = new ProgramCounter();
        private readonly ALU _alu = new ALU();

        public CPU()
        {
            PC = new bool[16];
            AddressM = new bool[16];
            OutM = new bool[16];
        }

        public void Compute(bool reset, bool[] instruction, bool[] inM)
        {
            var aRegMux = new Mux16Gate();
            var aluMux = new Mux16Gate();
            var andGate = new AndGate();
            var orGate = new OrGate();

            var isCInstruction = instruction[0];
            var canWriteM = instruction[12];

            andGate.Compute(instruction[11], isCInstruction);
            var canWriteD = andGate.Out;

            orGate.Compute(instruction[10], !isCInstruction);
            var canWriteA = orGate.Out;

            var canUseM = instruction[3];

            aRegMux.Compute(instruction, _alu.Out, isCInstruction);

            _aRegister.Compute(aRegMux.Out, canWriteA);

            aluMux.Compute(_aRegister.Out, inM, canUseM);

            _alu.Compute(_dRegister.Out, aluMux.Out,
                isCInstruction && instruction[4],
                isCInstruction && instruction[5],
                isCInstruction && instruction[6],
                isCInstruction && instruction[7],
                isCInstruction && instruction[8],
                isCInstruction && instruction[9]);

            _dRegister.Compute(_alu.Out, canWriteD);

            var shouldLoad = isCInstruction &&
                ((instruction[13] && instruction[14] && instruction[15]) //unconditional
                || (instruction[15] && !_alu.Ng && !_alu.Zr) // > 0
                || (instruction[14] && _alu.Zr) // == 0
                || (instruction[14] && instruction[15] && !_alu.Ng) // >= 0
                || (instruction[13] && _alu.Ng) // < 0
                || (instruction[13] && instruction[15] && !_alu.Zr) // !=0
                || (instruction[13] && instruction[14] && (_alu.Ng || _alu.Zr))); // <= 0

            var shouldIncrement = !shouldLoad;

            _pc.Compute(_aRegister.Out, shouldLoad, shouldIncrement, reset);

            //Outputs
            WriteM = isCInstruction && canWriteM;
            AddressM = _aRegister.Out;
            PC = _pc.Out;
            OutM = _alu.Out;
        }
    }
}
