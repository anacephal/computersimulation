﻿using System;
namespace CPU.Complex
{
    public enum AluOperations
    {
        Zero,
        One,
        MinusOne,
        X,
        Y,
        NotX,
        NotY,
        MinusX,
        MinusY,
        XPlusOne,
        YPlusOne,
        XMinusOne,
        YMinusOne,
        XPlusY,
        XMinusY,
        YMinusX,
        XAndY,
        XOrY
    }
}
