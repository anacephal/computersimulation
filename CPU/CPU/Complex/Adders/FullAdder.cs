﻿using System;
namespace CPU.Complex.Adders
{
    public class FullAdder
    {
        public bool Out { get; private set; }
        public bool Carry { get; private set; }

        public void Compute(bool a, bool b, bool c)
        {
            var ha1 = new HalfAdder();
            var ha2 = new HalfAdder();

            ha1.Compute(a, b);
            ha2.Compute(ha1.Out, c);

            Out = ha2.Out;
            Carry = ha1.Carry || ha2.Carry;
        }
    }
}
