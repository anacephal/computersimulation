﻿using System;
using CPU.Primitives;

namespace CPU.Complex
{
    public class HalfAdder
    {
        public bool Out { get; private set; }
        public bool Carry { get; private set; }

        public void Compute(bool a, bool b)
        {
            if (a && b)
            {
                Out = false;
                Carry = true;
            }
            else if ((a && !b) || (!a && b))
            {
                Out = true;
                Carry = false;
            }
            else
            {
                Out = false;
                Carry = false;
            }
        }
    }
}
