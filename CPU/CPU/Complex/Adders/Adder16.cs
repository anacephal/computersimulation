﻿using System;
using CPU.Primitives;

namespace CPU.Complex.Adders
{
    public class Adder16 : Chip
    {
        public void Compute(bool[] a, bool[] b)
        {
            var fa = new FullAdder();
            var result = new bool[16];
            for(var i=15; i >= 0; i--)
            {
                fa.Compute(a[i], b[i], fa.Carry);
                result[i] = fa.Out;
            }
            Out = result;
        }
    }
}
