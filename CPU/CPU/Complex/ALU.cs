﻿using System;
using System.Linq;
using CPU.Complex.Adders;
using CPU.Primitives;

namespace CPU.Complex
{
    public class ALU : Chip
    {
        public ALU()
        {
            Out = new bool[16];
        }

        public bool Zr { get; private set; }
        public bool Ng { get; private set; }

        public void Compute(bool[] x, bool[] y, bool zx, bool nx, bool zy, bool ny, bool f, bool no)
        {
            var not16 = new NotGate16();
            var not16y = new NotGate16();
            var mux16x = new Mux16Gate();
            var mux16y = new Mux16Gate();
            var mux16f = new Mux16Gate();
            var not16no = new NotGate16();
            var mux16no = new Mux16Gate();

            // presetting
            // zx
            mux16x.Compute(x, new bool[16], zx);

            // nx
            not16.Compute(mux16x.Out);
            mux16x.Compute(mux16x.Out, not16.Out, nx);

            //zy
            mux16y.Compute(y, new bool[16], zy);

            //ny
            not16y.Compute(mux16y.Out);
            mux16y.Compute(mux16y.Out, not16y.Out, ny);

            //f
            var adder = new Adder16();
            adder.Compute(mux16x.Out, mux16y.Out);

            var and16 = new AndGate16();
            and16.Compute(mux16x.Out, mux16y.Out);

            mux16f.Compute(and16.Out, adder.Out, f);

            //no
            not16no.Compute(mux16f.Out);
            mux16no.Compute(mux16f.Out, not16no.Out, no);

            //Out
            Out = mux16no.Out;

            // out is zero
            var andGate16 = new And16Gate();
            not16.Compute(Out);
            andGate16.Compute(not16.Out);
            Zr = andGate16.Out;

            // Out is negative
            Ng = Out[0];
        }
    }
}
