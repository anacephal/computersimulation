﻿using System;
using CPU.Primitives;

namespace CPU.Complex
{
    public class Computer : Chip
    {
        private readonly CPU _cpu;
        private readonly ROM16 _rom;
        private readonly Ram16k _ram;
        private bool _isReset = true;

        public Computer(bool[][] instructions, bool[][] ramState)
        {
            _cpu = new CPU();
            _rom = new ROM16(instructions);
            _ram = new Ram16k(ramState);
        }

        public void Compute()
        {
            var instrAddr = _cpu.PC;
            _rom.Compute(instrAddr);
            _cpu.Compute(_isReset, _rom.Out, _ram.Out);
            _ram.Compute(_cpu.OutM, _cpu.WriteM, _cpu.AddressM);
            _isReset = false;
            Out = _cpu.OutM;
        }
    }
}
