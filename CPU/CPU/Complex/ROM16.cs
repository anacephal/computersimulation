﻿using System;
using CPU.Complex.Adders;
using CPU.Primitives;

namespace CPU.Complex
{
    public class ROM16 : Chip
    {
        private readonly Ram16k _ram;

        public ROM16(bool[][] instructions)
        {
            _ram = new Ram16k(instructions);
        }

        public void Compute(bool[] pc)
        {
            _ram.Compute(new bool[16], false, pc);
            Out = _ram.Out;
        }
    }
}
