﻿using System;
using CPU.Primitives;

namespace CPU.Complex
{
    public class RamDecorator : Ram
    {
        Ram _ram;

        public override bool[] Out => _ram.Out;

        public RamDecorator(Ram ram)
        {
            _ram = ram;
        }

        public void Compute(bool[] data, bool load, string address)
        {
            _ram.Compute(data, load, BooleanHelper.ConvertToBool(address));
        }

        public void Compute(string data, bool load, string address)
        {
            _ram.Compute(BooleanHelper.ConvertToBool(data), load, BooleanHelper.ConvertToBool(address));
        }
    }
}
