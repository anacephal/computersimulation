﻿using System;
using CPU.Primitives;

namespace CPU.Complex
{
    public interface IRam
    {
        void Compute(bool[] data, bool load, bool[] address);
    }
}
