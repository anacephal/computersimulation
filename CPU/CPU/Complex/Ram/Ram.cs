﻿using System;
using System.Linq;
using CPU.Primitives;

namespace CPU.Complex
{
    public class Ram : Chip, IRam
    {
        public Register16[] _memory;

        public Ram()
        {

        }

        public Ram(int numOfRegisters)
        {
            _memory = Enumerable.Range(0, numOfRegisters)
                                .Select(i => new Register16())
                                .ToArray();
        }

        public void Compute(bool[] data, bool load, bool[] address)
        {
            var regAddr = ComputeRegisterAddress(address);
            var selectedRegister = _memory[regAddr];
            if(load)
            {
                selectedRegister.Compute(data, true);
            }
            Out = selectedRegister.Out;
        }

        private short ComputeRegisterAddress(bool[] address)
        {
            var reversed = address.Reverse().ToArray();
            short accum = 0;
            for (int i = 0; i < address.Length; i++) {
                accum = (short)(accum + (reversed[i] ? 1 : 0) * Math.Pow(2, i));
            }
            return accum;
        }
    }
}
