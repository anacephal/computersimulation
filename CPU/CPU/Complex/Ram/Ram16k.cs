﻿using System;
using CPU.Complex.Adders;

namespace CPU.Complex
{
    public class Ram16k : Ram
    {
        public Ram16k() : base(16384)
        {
            Out = new bool[16];
        }

        public Ram16k(bool[][] initialState): this()
        {
            var address = BooleanHelper.ConvertToBool("0000 0000 0000 0000");
            var increment = BooleanHelper.ConvertToBool("0000 0000 0000 0001");
            var adder16 = new Adder16();

            for (var i = 0; i < initialState.Length; i++)
            {
                this.Compute(initialState[i], true, address);
                adder16.Compute(address, increment);
                address = adder16.Out;
            }
        }
    }
}
