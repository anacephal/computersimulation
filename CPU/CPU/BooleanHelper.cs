﻿using System;
using System.Linq;

namespace CPU
{
    public static class BooleanHelper
    {
        public static bool[] ConvertToBool(short[] data)
        {
            return data.Select(v => v == 1).ToArray();
        }

        public static bool[] ConvertToBool(string data)
        {
            return data.Where(v => v == '1' || v == '0').Select(v => v == '1' ? true : false).ToArray();
        }

        public static bool[][] ConvertToInstructions(string programText)
        {
            var length = programText.Length;
            var splitInstructions = new string[length / 16];

            for (var i=0;i< length/16;i++)
            {
                splitInstructions[i] = programText.Substring(i * 16, 16);
            }

            return splitInstructions.Select(inst => ConvertToBool(inst)).ToArray();
        }

        public static short[] ConvertToShort(bool[] data)
        {
            return data.Select(v => v ? (short)1 : (short)0).ToArray();
        }

        public static string ConvertToString(bool[] data)
        {
            return data.Aggregate(string.Empty, (acc, val) => acc + (val ? "1" : "0"));
        }
    }
}
