﻿using System;
using System.Collections.Generic;
using CPU.Complex.Adders;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CPU.Tests
{
    [TestClass]
    public class Adder16Tests
    {
        [TestMethod]
        public void TestAdder16()
        {
            var adder = new Adder16();
            var a = new bool[] { false, false, false, false, true, true, true, true, false, false, false, false, true, true, true, true };
            var b = new bool[] { true, true, true, true, false, false, false, false, true, true, true, true, false, false, false, false };
            adder.Compute(a, b);
            var result = new bool[] { true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true };


            Assert.IsTrue(Helpers.AreSequenceEqual(result, adder.Out, (a,b) => a == b));
        }
    }
}
