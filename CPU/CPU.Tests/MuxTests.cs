﻿using System;
using CPU.Primitives;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CPU.Tests
{
    [TestClass]
    public class MuxTests
    {
        [TestMethod]
        [DataRow(false,false,false,false)]
        [DataRow(false,true,false,false)]
        [DataRow(true,false,false,true)]
        [DataRow(true,true,false,true)]
        [DataRow(false,false,true,false)]
        [DataRow(false,true,true,true)]
        [DataRow(true,false,true,false)]
        [DataRow(true,true,true,true)]
        public void TestMux(bool a, bool b, bool sel, bool res)
        {
            //arrange
            var mux = new MuxGate();

            // act
            mux.Compute(a, b, sel);

            //assert
            Assert.AreEqual(mux.Out, res);
        }
    }
}
