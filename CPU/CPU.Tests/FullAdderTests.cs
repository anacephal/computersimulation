using CPU.Complex.Adders;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CPU.Tests
{
    [TestClass]
    public class FullAdderTests
    {
        [TestMethod]
        [DataRow(true, true, false, false, true)]
        [DataRow(true, true, true, true, true)]
        [DataRow(false, true, false, true, false)]
        [DataRow(false, true, true, false, true)]
        [DataRow(true, false, false, true, false)]
        [DataRow(true, false, true, false, true)]
        [DataRow(false, false, false, false, false)]
        [DataRow(false, false, true, true, false)]
        public void TestFullAdder(bool a, bool b, bool c, bool @out, bool s)
        {
            var fa = new FullAdder();
            fa.Compute(a, b, c);
            Assert.AreEqual(fa.Out, @out);
            Assert.AreEqual(fa.Carry, s);
        }
    }
}
