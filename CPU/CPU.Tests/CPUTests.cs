﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CPU.Tests
{
    [TestClass]
	public class CPUTests
	{
        
        [TestMethod]
        //no jump
        [DataRow(false,false,false,false,false,false)]
        [DataRow(false,false,false,false,true,false)]
        [DataRow(false,false,false,true,false,false)]
        //> 0
        [DataRow(false,false,true,false,false,true)]
        [DataRow(false,false,true,false,true,false)]
        [DataRow(false,false,true,true,false,false)]
        //=0
        [DataRow(false,true,false,false,false,false)]
        [DataRow(false,true,false,false,true,false)]
        [DataRow(false,true,false,true,false,true)]
        //>=0
        [DataRow(false,true,true,false,false,true)]
        [DataRow(false,true,true,false,true,false)]
        [DataRow(false,true,true,true,false,true)]
        //<0
        [DataRow(true,false,false,false,false,false)]
        [DataRow(true,false,false,false,true,true)]
        [DataRow(true,false,false,true,false,false)]
        //!=0
        [DataRow(true,false,true,false,false,true)]
        [DataRow(true,false,true,false,true,true)]
        [DataRow(true,false,true,true,false,false)]
        //<=0
        [DataRow(true,true,false,false,false,false)]
        [DataRow(true,true,false,false,true,true)]
        [DataRow(true,true,false,true,false,true)]
        //unconditional
        [DataRow(true,true,true,false,false,true)]
        [DataRow(true,true,true,false,true,true)]
        [DataRow(true,true,true,true,false,true)]
        public void TestJump(bool left, bool mid, bool right, bool Zr, bool Ng, bool result)
        {
            //var res = (rightBit && !isNeg)
            //       && (leftBit && (!isZero && !isNeg))
            //       && (!midBit && isZero);
            var shouldLoad =
                (left && mid && right) //unconditional
                || (right && !Ng && !Zr) // > 0
                || (mid && Zr) // == 0
                || (mid && right && !Ng) // >= 0
                || (left && Ng) // < 0
                || (left && right && !Zr) // !=0
                || (left && mid && (Ng || Zr)); // <= 0

            Assert.AreEqual(result, shouldLoad);
        }
		
	}
}
