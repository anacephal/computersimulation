﻿using System;
using CPU.Complex;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CPU.Tests
{
    [TestClass]
    public class ProgramCounterTests
    {
        [TestMethod]
        public void Test_Increment()
        {
            // arrange
            var pc = new ProgramCounter();
            var initialVal = BooleanHelper.ConvertToBool(new short[] {
                0, 0, 0, 0,
                1, 0, 1, 0,
                0, 1, 1, 1,
                0, 0, 1, 0
            });
            var expectedValue = BooleanHelper.ConvertToBool(new short[]
            {
                0, 0, 0, 0,
                1, 0, 1, 0,
                0, 1, 1, 1,
                0, 1, 0, 0
            });

            // act
            pc.Compute(initialVal, true, false, false);
            pc.Compute(new bool[16], false, true, false);
            pc.Compute(new bool[16], false, true, false);

            // assert
            Assert.IsTrue(Helpers.AreSequenceEqual(expectedValue, pc.Out, (a, b) => a == b));
        }

        [TestMethod]
        public void Test_Load()
        {
            // arrange
            var pc = new ProgramCounter();
            var initialVal = BooleanHelper.ConvertToBool(new short[] {
                0, 0, 0, 0,
                1, 0, 1, 0,
                0, 1, 1, 1,
                0, 0, 1, 0
            });

            var expectedValue = BooleanHelper.ConvertToBool(new short[]
            {
                0, 0, 0, 0,
                1, 0, 1, 0,
                0, 1, 1, 1,
                0, 0, 1, 0
            });

            // act
            pc.Compute(initialVal, true, false, false);

            // assert
            Assert.IsTrue(Helpers.AreSequenceEqual(expectedValue, pc.Out, (a, b) => a == b));
        }

        [TestMethod]
        public void Test_Reset()
        {
            // arrange
            var pc = new ProgramCounter();
            var initialVal = BooleanHelper.ConvertToBool(new short[] {
                0, 0, 0, 0,
                1, 0, 1, 0,
                0, 1, 1, 1,
                0, 0, 1, 0
            });
            var expectedValue = new bool[16];

            // act
            pc.Compute(initialVal, true, false, false);
            pc.Compute(initialVal, false, false, true);

            // assert
            Assert.IsTrue(Helpers.AreSequenceEqual(expectedValue, pc.Out, (a, b) => a == b));
        }
    }
}
